# WPHomePosts

[![BMAC](https://img.shields.io/badge/Donate-Buy%20Me%20A%20Coffee-orange.svg?style=flat-square)](https://www.buymeacoffee.com/Occhioverde03)

Super simple plugin that can show the latest posts on the homepage. Initially written for the [UnParcoPerLaCittà.it](https://www.unparcoperlacitta.it) committee and based on [the snippet](https://codepen.io/mithicher/pen/azQKNN) made by Mithicher.

## Install me on your WordPress website!

### Download and package

You can install WPHomePosts starting directly from its source code in two easy steps. First, clone the official repo on your computer:

```Sh
git clone https://gitlab.com/nexxontech/wordpress-plugins/wphomeposts.git
```

This command will create a folder named WPHomePosts; compress it using the zip command:

```Sh
zip -r WPHomePosts.zip WPHomePosts/
```

At this point you can upload the resulting package directly to your WordPress bulletin board. You can find more detailed instructions [here](https://wordpress.org/support/article/managing-plugins/#manual-upload-via-wordpress-admin).

### Usage
WPHomePosts is super easy to use and you should be able to set it up in seconds.
To view and card with the most recent posts in a category, simply go to your homepage customization and, here, paste the following shortcode:

```
[homePosts cat={}]
```

Be sure to replace the brackets with the ID of the category from which you want to load the posts that will be shown.

## Help us to translate and improve the application

### Edit the code

If you want to make some changes to the application code you just have to fork this repository, clone it on you device and edit the files. Then, you can open a Pull Request and we'll evaluate it.
