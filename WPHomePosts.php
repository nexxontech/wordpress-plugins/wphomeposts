<?php
/**
* Plugin Name: WPHomePosts
* Description: Super simple plugin that can show the latest posts on the homepage.
* Version: 1.0.0
* Requires at least: 5.3
* Requires PHP: 7.2
* Author: NexxonTech (Riccardo Sacchetto)
* Author URI: https://www.nexxontech.it
* License: GPLv3 or later
*/

/**
 * Copyright (c) 2021 by Riccardo Sacchetto (https://rsacchetto.nexxontech.it)
 * Copyright (c) 2021 by Mithicher (https://codepen.io/mithicher/pen/azQKNN)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Function homePosts: Returns the HTML code with the latest posts to display instead of the shortcode [homePosts cat={}]
 *
 * @param $atts Attributes received from WordPress
 * @return $output HTML code to display
*/
function homePosts($atts) {
  // Get the atts
  $a = shortcode_atts(array(
    'cat' => null
  ), $atts);

  // If the user provided a category
  if (!empty($a['cat'])) {
    // Get the posts from WP backend
    $posts = get_posts(array(
      'numberposts' => 3,
      'category' => $a['cat']
    ));
    // Create the $output variable
    $output = '<div class="container"><div class="row">';

    // Iterate through the posts I got from WP
    foreach($posts as $post) {
      // Get the post featured image
      $image = get_the_post_thumbnail($post->ID, array(
        'class' => 'border-tlr-radius'
      ));
      // Get the post content (text only)
      $content = wp_strip_all_tags(get_the_content("[...]", false, $post));
      // Get the post category
      $category = get_the_category($post->ID)[0];
      // Get the post date
      $date = date_parse($post->post_date);
      // Get the post permalink
      $permalink = get_post_permalink($post);

      // Generate the HTML card starting from the post info
      $output = $output . '
      <div class="col-md-4">
        <div class="card radius shadowDepth1">
          <div class="card__image border-tlr-radius">
            ' . $image . '
          </div>

          <div class="card__content card__padding">
            <div class="card__share">
              <a id="share" class="share-toggle share-icon" href="' . $permalink . '"></a>
            </div>
            <div class="card__meta">
              <a href="category/' . $category->slug . '">' . $category->name . '</a>
              <time>' . $date["day"] . '/' . $date["month"] . '/' . $date["year"] . '</time>
            </div>

            <article class="card__article">
              <h2><a href="' . $permalink . '">' . $post->post_title . '</a></h2>

              <p>' . $content . '</p>
            </article>
          </div>

          <div class="card__action"></div>
         </div>
       </div>
    ';
    }

    // Add the div closing tags and return $output
    return $output . '</div></div>';
  } else {
    // Return error
    return '<p>Error: No categoy provided</p>';
  }
}

/**
 * Function setupScripts: Registers and loads the required CSS stylesheets.
*/
function setupScripts() {
  // Get the plugin folder to reach the res folder
  $pluginUrl = plugin_dir_url( __FILE__ );

  // Register the CSS stylesheets
  wp_register_style('homepostsStyle', $pluginUrl . 'res/css/style.css' );
  wp_register_style('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css');

  // Load the CSS stylesheets
  wp_enqueue_style('homepostsStyle');
  wp_enqueue_style('bootstrap');
}

// Load the CSS files
add_action('wp_enqueue_scripts', 'setupScripts');

// Add the [homePosts cat={}] shortcode
add_shortcode('homePosts', 'homePosts');
?>
